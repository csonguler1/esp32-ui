import React, { Component } from 'react';
import { LineChart, Line, XAxis, YAxis, Label, Tooltip, ResponsiveContainer } from 'recharts';
import Title from './Title';
import moment from 'moment';

import API from '../components/Api';
import theme from '../theme';

const dateLongFormatter = (item) => moment(item).format("DD-MM-YY HH:mm:ss")
const dateShortFormatter = (item) => moment(item).format("DD-MM HH:mm")


class Chart extends Component {
  constructor(props){
    super(props);

    this.state = {
      temperature: [],
      vibration: []
    }
  }

  update = () =>{
    API.get(`timeseries/myesp32`,{
      params: {
        from: moment().subtract(15,"minutes").toISOString(),
        to: moment().toISOString(),
        sensorType: 'vibration',
        limit: 1000,
        sort: 'asc'
      }
    })
    .then(res => {
      this.setState({
        vibration: res.data
      });
    });
    API.get(`timeseries/myesp32`,{
      params: {
        from: moment().subtract(15,"minutes").toISOString(),
        to: moment().toISOString(),
        sensorType: 'temperature',
        limit: 1000,
        sort: 'asc'
      }
    })
    .then(res => {
      this.setState({
        temperature: res.data
      });
    });
  }

  componentDidMount = () => {
    this.update();
    this.timer = setInterval(this.update, 10000);
  }

  componentWillUnmount = () => {
    clearInterval(this.timer)
  }

  render() {
    let now = new Date().toLocaleTimeString();
    return (
      <React.Fragment>
        <Title>Today</Title>
        <ResponsiveContainer>
          <LineChart
            data={this.state.vibration}
            margin={{
              top: 16,
              right: 16,
              bottom: 0,
              left: 24,
            }}
          >
            <XAxis dataKey="ts" stroke={theme.palette.text.secondary}  tickFormatter={dateShortFormatter}/>
            <YAxis stroke={theme.palette.text.secondary}>
              <Label
                angle={270}
                position="left"
                style={{ textAnchor: 'middle', fill: theme.palette.text.primary }}
              >
                Vibration
              </Label>
            </YAxis>
            <Tooltip labelFormatter={dateLongFormatter} />
            <Line type="monotone" dataKey="V1" stroke={theme.palette.primary.line1} dot={false} />
            <Line type="monotone" dataKey="V2" stroke={theme.palette.primary.line2} dot={false} />
          </LineChart>
        </ResponsiveContainer>
        <ResponsiveContainer>
          <LineChart
            data={this.state.temperature}
            margin={{
              top: 16,
              right: 16,
              bottom: 0,
              left: 24,
            }}
          >
            <XAxis dataKey="ts" stroke={theme.palette.text.secondary}  tickFormatter={dateShortFormatter}/>
            <YAxis stroke={theme.palette.text.secondary}>
              <Label
                angle={270}
                position="left"
                style={{ textAnchor: 'middle', fill: theme.palette.text.primary }}
              >
                Temperature
              </Label>
            </YAxis>
            <Tooltip labelFormatter={dateLongFormatter} />
            <Line type="monotone" dataKey="T1" stroke={theme.palette.primary.line1} dot={false} />
            <Line type="monotone" dataKey="T2" stroke={theme.palette.primary.line2} dot={false} />
            <Line type="monotone" dataKey="T3" stroke={theme.palette.primary.line3} dot={false} />
            <Line type="monotone" dataKey="T4" stroke={theme.palette.primary.line4} dot={false} />
            <Line type="monotone" dataKey="T5" stroke={theme.palette.primary.line5} dot={false} />
            <Line type="monotone" dataKey="T6" stroke={theme.palette.primary.line6} dot={false} />
          </LineChart>
        </ResponsiveContainer>
        Last Updated {now}
      </React.Fragment>
    );
  }
}

export default Chart;
