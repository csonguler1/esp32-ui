import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Title from './Title';

const useStyles = makeStyles({
  infoText: {
    marginTop: 10,
    textAlign: "center"
  },
});

export default function KPI({header, value}) {
  const classes = useStyles();
  return (
    <React.Fragment>
      <Title>{header}</Title>
      <Typography align="center" component="p" variant="h4">
        {value}
      </Typography>
      <Typography component="p" variant="body1"  className={classes.infoText}>
        Last 15 minutes
      </Typography>
    </React.Fragment>
  );
}
