import React from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Title from './Title';

export default function Copyright() {
  const theme = useTheme();
  return (
    <Box pt={4}>
      <Typography variant="body2" color="textSecondary" align="center">
          <Link color="primary" href="#" onClick={preventDefault}>
            www.songuler.com
          </Link>
        {'Copyright © '}
          Cuneyt Songuler
        {new Date().getFullYear()}
        {'.'}
      </Typography>
    </Box>
  );
}
