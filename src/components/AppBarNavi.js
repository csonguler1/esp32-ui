import React from 'react';
import clsx from 'clsx';

import { createBrowserHistory } from "history";

import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import CastConnectedIcon from '@material-ui/icons/CastConnected';
import DashboardIcon from '@material-ui/icons/Dashboard';
import AssessmentIcon from '@material-ui/icons/Assessment';
import SettingsIcon from '@material-ui/icons/Settings';
import Dashboard from '../pages/Dashboard';
import Configuration from '../pages/Configuration';
import Report from '../pages/Report';
import {
  Router,
  Route,
  Link
} from "react-router-dom";

import useStyles from '../styles';

const history = createBrowserHistory();

export default function AppBarNavi() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
  console.log(open);
  const handleDrawerOpen = () => {
    console.log("handle open");
    setOpen(true);
    console.log(open);
  };
  const handleDrawerClose = () => {
    console.log("handle close");
    setOpen(false);
    console.log(open);
  };
 

  return (
    <div className={classes.root}>
      <AppBar position="absolute" className={clsx(classes.appBar, open && classes.appBarShift)}>
        <Toolbar className={classes.toolbar}>
          <IconButton
            edge="start"
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            className={clsx(classes.menuButton, open && classes.menuButtonHidden)}
          >
            <MenuIcon />
          </IconButton>
          <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
            ESP32 IOT
          </Typography>
          <IconButton color="inherit">
            <CastConnectedIcon />
            myesp32
          </IconButton>
        </Toolbar>
      </AppBar> 
      <Router history={history}>
        <Drawer
          variant="permanent"
          classes={{
            paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
          }}
          open={open}
        >
          <div className={classes.toolbarIcon}>
            <IconButton onClick={handleDrawerClose}>
              <ChevronLeftIcon />
            </IconButton>
          </div>
          <Divider />
          <List>
            <ListItem button component={Link} to="/">
              <ListItemIcon>
                <DashboardIcon />
              </ListItemIcon>
              <ListItemText primary="Dashboard" />
            </ListItem>
            <ListItem button component={Link} to="/report">
              <ListItemIcon>
                <AssessmentIcon />
              </ListItemIcon>
              <ListItemText primary="Report" />
            </ListItem>
            <ListItem button component={Link} to="/config">
              <ListItemIcon>
                <SettingsIcon />
              </ListItemIcon>
              <ListItemText primary="Configuration" />
            </ListItem>
          </List>
        </Drawer>
        <main className={classes.content}>
          <Route exact path="/" component={Dashboard} />
          <Route path="/report" component={Report} />
          <Route path="/config" component={Configuration} />
        </main>
      </Router>
    </div>
  );
}
