import React, { Component } from 'react';
import Link from '@material-ui/core/Link';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Title from './Title';

import moment from 'moment';

import API from '../components/Api';

class Alarms extends Component {
  constructor(props){
    super(props);

    this.state = {
      events: []
    }
  }

  update = () =>{
    API.get(`events/myesp32`,{
      params: {
        from: moment().subtract(15,"minutes").toISOString(),
        to: moment().toISOString(),
        limit: 1000
      }
    })
    .then(res => {
      this.setState({
        events: res.data
      });
    });
  }

  componentDidMount = () => {
    this.update();
    this.timer = setInterval(this.update, 10000);
  }

  componentWillUnmount = () => {
    clearInterval(this.timer)
  }

  render() {
    return (
    <React.Fragment>
      <Title>Recent Alarms</Title>
      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell>Date</TableCell>
            <TableCell>Description</TableCell>
            <TableCell>Severity</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {this.state.events.map((event) => (
            <TableRow key={event.id}>
              <TableCell>{event.ts}</TableCell>
              <TableCell>{event.description}</TableCell>
              <TableCell>{event.severity}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <div>
        <Link color="primary" href="#">
          See more alarms
        </Link>
      </div>
    </React.Fragment>
  );
          }
}

export default Alarms;