import React from 'react';
import useStyles from '../styles';

import Container from '@material-ui/core/Container';

export default function Configuration() {
  const classes = useStyles();

  return (
    <React.Fragment>
        <div className={classes.appBarSpacer} />
        <Container maxWidth="lg" className={classes.container}>
            Hello Config
        </Container>
    </React.Fragment>
  );
}
