import React, { Component } from 'react';
import clsx from 'clsx';

import Container from '@material-ui/core/Container';
import { MuiPickersUtilsProvider, KeyboardDateTimePicker } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import { withStyles } from '@material-ui/core/styles';
import { LineChart, Line, XAxis, YAxis, Tooltip, Label, ResponsiveContainer } from 'recharts';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import moment from 'moment';

import API from '../components/Api';
import AppBarSpacer from '../components/AppBarSpacer';
import Title from '../components/Title';
import theme from '../theme';

const dateLongFormatter = (item) => moment(item).format("DD-MM-YY HH:mm:ss")
const dateShortFormatter = (item) => moment(item).format("DD-MM HH:mm")

const styles = theme => ({
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },

});

class Report extends Component {
  constructor(props){
    super(props);

    this.state = {
      from: moment().add(-1, 'days'),
      to: moment(),
      temperature: [],
      vibration: []
    }
  }
  query = () => {

    if(this.state.from.isValid() && this.state.to.isValid()){
      API.get(`aggregates/myesp32`,{
        params: {
          from: this.state.from.toISOString(),
          to: this.state.to.toISOString(),
          limit: 100
        }
      })
      .then(res => {
        let timeseries = [];
        res.data.forEach((item) => {
          console.log(JSON.stringify(item));
          timeseries.push({ts: item.T1.firstts, T1: item.T1.first});
          timeseries.push({ts: item.T1.mints, T1: item.T1.min});
          timeseries.push({ts: item.T1.maxts, T1: item.T1.max});
          timeseries.push({ts: item.T1.lastts, T1: item.T1.last});
        }
        )
        this.setState({ temperature: timeseries });
      })

      API.get(`aggregates/myesp32`,{
        params: {
          from: this.state.from.toISOString(),
          to: this.state.to.toISOString(),
          limit: 100
        }
      })
      .then(res => {
        let timeseries = [];
        res.data.forEach((item) => {
          console.log(JSON.stringify(item));
          timeseries.push({ts: item.V1.firstts, V1: item.V1.first});
          timeseries.push({ts: item.V1.mints, V1: item.V1.min});
          timeseries.push({ts: item.V1.maxts, V1: item.V1.max});
          timeseries.push({ts: item.V1.lastts, V1: item.V1.last});
        }
        )
        this.setState({ temperature: timeseries });
       
      })
    }
  }
  componentDidMount() {
    this.query();
  }

  handleFrom = (date) => {
    this.setState({
      from: date
    });
  }
  handleTo = (date) => {
    this.setState({
      to: date
    });
  }
  render() {
    const fixedHeightPaper = clsx(this.props.classes.paper, this.props.classes.fixedHeight);
    return(
      <React.Fragment>
        <AppBarSpacer />
        <Container maxWidth="lg" className={this.props.classes.container}>
            <Grid container spacing={3}>
              <Grid item xs={12} md={4} lg={3}>
                  <Paper className={fixedHeightPaper}>    
                    <MuiPickersUtilsProvider utils={MomentUtils}>
                      <Title>From</Title>
                        <KeyboardDateTimePicker
                          variant="inline"
                          ampm={false}
                          value={this.state.from}
                          onChange={this.handleFrom}
                          onError={console.log}
                          format="DD-MM-YY HH:mm:ss"
                        />
                      <Title>To</Title>
                      <KeyboardDateTimePicker
                          variant="inline"
                          ampm={false}
                          value={this.state.to}
                          onChange={this.handleTo}
                          onError={console.log}
                          format="DD-MM-YY HH:mm:ss"
                        />
                    </MuiPickersUtilsProvider>
                    <Divider></Divider>
                    <Button variant="contained" color="primary" onClick={this.query}>
                      Query
                    </Button>
                  </Paper>
              </Grid>
                  {/* Chart */}
              <Grid item xs={12} md={8} lg={9}>
              <Paper className={fixedHeightPaper}>
                <ResponsiveContainer>
                  <LineChart
                    data={this.state.temperature}
                    margin={{
                      top: 16,
                      right: 16,
                      bottom: 0,
                      left: 24,
                    }}
                  >
                    <XAxis dataKey="ts" stroke={theme.palette.text.secondary} tickFormatter={dateShortFormatter} />
                    <YAxis stroke={theme.palette.text.secondary}>
                      <Label
                        angle={270}
                        position="left"
                        style={{ textAnchor: 'middle', fill: theme.palette.text.primary }}
                      >
                        Temperature
                      </Label>
                    </YAxis>
                    <Tooltip labelFormatter={dateLongFormatter} />
                    <Line type="monotone" dataKey="T1" stroke={theme.palette.primary.line1} dot={false} />
                  </LineChart>
                </ResponsiveContainer>
              </Paper>
              </Grid>
              <Grid item xs={12} md={4} lg={3}>
              <Paper className={fixedHeightPaper}></Paper>
              </Grid>
              <Grid item xs={12} md={8} lg={9} >
                <Paper className={fixedHeightPaper}>
                  <ResponsiveContainer>
                    <LineChart
                      data={this.state.vibration}
                      margin={{
                        top: 16,
                        right: 16,
                        bottom: 0,
                        left: 24,
                      }}
                    >
                      <XAxis dataKey="ts" stroke={theme.palette.text.secondary} tickFormatter={dateShortFormatter} />
                      <YAxis stroke={theme.palette.text.secondary}>
                        <Label
                          angle={270}
                          position="left"
                          style={{ textAnchor: 'middle', fill: theme.palette.text.primary }}
                        >
                          Vibration
                        </Label>
                      </YAxis>
                      <Tooltip labelFormatter={dateLongFormatter} />
                      <Line type="monotone" dataKey="V1" stroke={theme.palette.primary.line1} dot={false} />
                    </LineChart>
                  </ResponsiveContainer>
                </Paper>
                </Grid>
            </Grid>
        </Container>
    </React.Fragment>
    );
  }
}

export default withStyles(styles)(Report);
