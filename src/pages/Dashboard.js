import React from 'react';
import clsx from 'clsx';

import useStyles from '../styles';

import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

import Chart from '../components/Chart';
import KPI from '../components/KPI';
import Alarms from '../components/Alarms';

export default function Dashboard(){
    const classes = useStyles();
    const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

    return(
    <React.Fragment>
        <div className={classes.appBarSpacer} />
        <Container maxWidth="lg" className={classes.container}>
            <Grid container spacing={3}>
                {/* Chart */}
                <Grid item xs={12} md={8} lg={9}>
                <Paper className={fixedHeightPaper}>
                    <Chart />
                </Paper>
                </Grid>
                {/* KPI */}
                <Grid item xs={12} md={4} lg={3}>
                <Paper className={fixedHeightPaper}>
                    <KPI header="Average Temperature" value="29 C"/>
                </Paper>
                </Grid>
                {/* Recent Alarms */}
                <Grid item xs={12}>
                <Paper className={classes.paper}>
                    <Alarms />
                </Paper>
                </Grid>
            </Grid>
        </Container>
    </React.Fragment>
    );
}