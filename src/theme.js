import { red, blue, green, purple, brown, grey } from '@material-ui/core/colors';
import { createMuiTheme } from '@material-ui/core/styles';

// A custom theme for this app
const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#556cd6',
      line1: red.A400,
      line2: blue.A400,
      line3: green.A400,
      line4: purple.A400,
      line5: brown.A400,
      line6: grey.A400
    },
    secondary: {
      main: '#19857b',
    },
    error: {
      main: red.A400,
    },
    background: {
      default: '#fff',
    },
  },
});

export default theme;
